# Работа с базами данных в Java

## СУБД

* Система управления базами данных
    - Серверная программа
    - Хранилище (различные файлы)
    - Инструмент администратора (например, pgAdmin)
    - Возможность подключения из сторонних приложений

[SQL-книга]()

### Реляционные SQL-базы данных

* Такие базы данных состоят из таблиц
* Таблицы могут быть друг с другом связаны
* Все манипуляции проходят с помощью языка SQL

### Для чего?

* Обеспечивают эффективное хранение данных
* Эффективное получение данных
* Целостность данных - не может быть ситуации, что вы удалили курс, а в курсе были студенты

### Различные дистрибутивы и диалекты SQL

* Oracle
* PostgreSQL (обязательно запомнить пароль при установке!!!)
* MySQL
* SQLite
* H2
* Microsoft .NET

### Таблицы

* Строки и столбцы
* Столбец - название, тип, дополнительные ограничения
* Строка - конкретные значения столбцов для одной записи
* Первичный ключ - особый тип ключа, который является идентификатором строки (+ семантически), используется для связей
* Внешний ключ - ссылка на другую колонку, значение ключа обязательно должно указывать на существующую запись

## Связи между таблицами

* Один-ко-многим (One-To-Many) - в курсе есть много уроков. Нужно у урока сделать внешний ключ, ссылающийся на id-курса.
* Многие-ко-многим (Many-To-Many) - например, у студента есть много курсов, а у курса есть много студентов. Нужно создать третью таблицу с двумя внешними ключами.

## JDBC

* JDBC - Java DataBase Connectivity - технология (стандарт) который описывает механики работы с базами данных в Java.
* Представлен пакетом `java.sql`.
* Центральный элемент технологии JDBC - концепция драйверов. Поскольку существует множество различных СУБД, а Java - одна, 
то для каждой СУБД реализован так называемый "драйвер", который позволяет одному и тому же Java-коду работать с разными типами СУБД.

### Компоненты JDBC

* `DriverManager` - класс, который занимается поиском подходящего драйвера для подключения к базе данных среди доступных библиотек.
* `Connection` - интерфейс, объекты которого описывают подключение к базе к данных.
* `Statement` - интерфейс, объекты которого могут направлять запросы в базу.
* `ResultSet` - интерфейс, объекты которого из представляют из себя курсор (итератор), который пробегает результирующее множество
* `DataSource` - интерфейс, объекты которого представляют `Connection`