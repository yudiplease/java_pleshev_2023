package ru.itis.repositories;

import ru.itis.models.Student;

import java.util.List;
import java.util.Optional;

public interface StudentsRepository {
    List<Student> findAll();

    List<Student> findAllByAgeGreaterThanOrderByIdDesc(Integer minAge);

    void save(Student student);

    void update(Student student);

    void delete(Long id);

    Optional<Student> findById(Long id);

}
