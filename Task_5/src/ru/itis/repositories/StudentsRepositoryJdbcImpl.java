package ru.itis.repositories;

import ru.itis.models.Student;

import javax.sql.DataSource;
import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.function.Function;

public class StudentsRepositoryJdbcImpl implements StudentsRepository {



    //language=SQL
    private static final String SQL_SELECT_ALL_STUDENTS = "select * from student;";

    //language=SQL
    private static final String SQL_INSERT_USER = "insert into student(first_name, last_name, email, password) " +
            "values (?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from student where id = ?";

    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update student set first_name = ?, last_name = ?, age = ?, phone_number = ?, email = ?, password = ? where id = ?";

    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from student where id = ?";

    //language=SQL
    private static final String SQL_SELECT_STUDENTS_SORT = "select * from student where age > ? order by id desc";

    private static final Function<ResultSet, Student> studentMapper = row -> {

        try {
            Long id = row.getLong("id");
            String firstName = row.getString("first_name");
            String lastName = row.getString("last_name");
            Integer age = row.getObject("age", Integer.class);
            return new Student(id, firstName, lastName, age);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    };

    private final DataSource dataSource;

    public StudentsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Student> findAll() {

        List<Student> students = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_STUDENTS)) {

                while (resultSet.next()) {
                    students.add(studentMapper.apply(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        return students;
    }

    @Override
    public List<Student> findAllByAgeGreaterThanOrderByIdDesc(Integer minAge) {

        List<Student> students = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_STUDENTS_SORT)) {
            preparedStatement.setInt(1,minAge);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                students.add(studentMapper.apply(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return students;
    }

    @Override
    public void save(Student student) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, student.getFirstName());
            statement.setString(2, student.getLastName());
            statement.setString(3, student.getEmail());
            statement.setString(4, student.getPassword());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't save user");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                student.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException("Can't obtain generated key");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Student student) {
        Scanner scanner = new Scanner(System.in);

        String newFirstName = scanner.nextLine();
        String newLastName = scanner.nextLine();
        String newAgeString = scanner.nextLine();
        Integer newAge = Integer.parseInt(newAgeString);
        String newPhoneNumber = scanner.nextLine();
        String newEmail = scanner.nextLine();
        String newPassword = scanner.nextLine();

        Long id = student.getId();

        try (Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_BY_ID)) {

            statement.setString(1, newFirstName);
            statement.setString(2, newLastName);
            statement.setInt(3, newAge);
            statement.setString(4, newPhoneNumber);
            statement.setString(5, newEmail);
            statement.setString(6, newPassword);
            statement.setLong(7,id);
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update user");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Long id) {
        try (Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID)) {
            statement.setLong(1, id);
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't delete user");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Student> findById(Long id) {

        try (Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Student student = studentMapper.apply(resultSet);
                    return Optional.of(student);
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


}
