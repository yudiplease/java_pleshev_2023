package ru.itis.repositories;

import ru.itis.models.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class StudentsRepositoryJdbcImpl implements StudentsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_STUDENTS = "select * from student;";

    private static final Function<ResultSet, Student> studentMapper = row -> {

        try {
            Long id = row.getLong("id");
            String firstName = row.getString("first_name");
            String lastName = row.getString("last_name");
            Integer age = row.getObject("age", Integer.class);
            return new Student(id, firstName, lastName, age);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    };

    private final DataSource dataSource;

    public StudentsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Student> findAll() {

        List<Student> students = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_STUDENTS)) {
                while (resultSet.next()) {
                    students.add(studentMapper.apply(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        return students;
    }

    @Override
    public void save(Student student) {
        String firstName = student.getFirstName();
        String lastName = student.getLastName();
        Integer age = student.getAge();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into student(first_name, last_name, age) values (?, ?, ?);");
            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, lastName);
            preparedStatement.setInt(3, age);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Student> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_STUDENTS)) {
                while (resultSet.next()) {
                    if (resultSet.getLong("id") == id) {
                        Optional<Student> student = Optional.of(new Student(id, resultSet.getString("first_name"),
                                resultSet.getString("last_name"), resultSet.getInt("age")));
                        return student;
                    }
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return Optional.empty();
    }
}
