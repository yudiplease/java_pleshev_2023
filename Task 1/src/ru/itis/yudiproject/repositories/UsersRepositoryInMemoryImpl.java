package ru.itis.yudiproject.repositories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ru.itis.yudiproject.models.User;

public class UsersRepositoryInMemoryImpl implements UsersRepository {

    private final Map<Long, User> users;
    private long counter;

    public UsersRepositoryInMemoryImpl() {
        this.users = new HashMap<>();
        this.counter = 0;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public void save(User entity) {
        users.put(counter, entity);
        entity.setId(counter);
        counter++;
    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void delete(User entity) {

    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public User findById(Long aLong) {
        return null;
    }
}