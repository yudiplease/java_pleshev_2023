-- 1
select model, speed, hd from pc where price < 500;

-- 2
select maker from product where product.type = 'printer' group by maker;

-- 3
select model, ram, screen from laptop where price > 1000;

-- 4
select * from printer where color = 'y';

-- 5
select model, speed, hd from pc where (cd = '12x' OR cd = '24x') and price < 600;

-- 6
Select maker, speed from product inner join laptop on product.model = laptop.model where hd >= 10;

-- 7
select laptop.model, laptop.price
from laptop
    inner join product on laptop.model = product.model
where maker = 'B'

union

select pc.model, pc.price
from pc
    inner join product on pc.model = product.model
where maker = 'B'

union

select printer.model, printer.price
from printer
    inner join product on printer.model = product.model
where maker = 'B';

-- 8
select maker
from product
where type = 'pc' and maker not in
                      ( select maker
                      from product
                      where type = 'Laptop')
group by maker;

-- 9
select maker
from pc
    inner join product
        on pc.model = product.model
where speed >= 450
group by maker;

-- 10
select model, price
from printer
where price = (select max(price) from printer);

-- 11
select avg(speed)
from pc;

-- 12
select avg(speed)
from laptop
where price > 1000;

-- 13
select avg(speed)
from pc
    inner join product on product.model = pc.model
where maker = 'A'
group by maker;

-- 14
select classes.class, name, country
from classes
    inner join ships on classes.class = ships.class
where numguns >= 10;

-- 15
select hd
from pc
group by hd
having count(model) > 1;

-- 16
select distinct b.model as model, a.model as model, a.speed, a.ram
from pc as a, pc b
where a.speed = b.speed and a.ram = b.ram and a.model < b.model;

-- 17
select distinct type, laptop.model, speed
from laptop
    inner join product on laptop.model = product.model
where speed < (select min(speed) from pc);

-- 18
select distinct maker,price
from printer
    inner join product on printer.model= product.model
where price = (select min(price)
from printer
where color = 'y' ) and color = 'y';

-- 19
select maker ,avg(screen)as avg_screen
from laptop
    inner join product on laptop.model =  product.model
group by maker;

-- 20
select maker , count(model) as Count_Model
from product
where type = 'pc'
group by maker
having count(model) >= 3;

-- 21
select maker , max(price)as Max_price
from pc
    inner join product on pc.model= product.model
group by maker;

-- 22
select speed , avg(price) as Avg_price
from pc
where speed > 600
group by speed;

-- 23
select distinct maker  from pc inner join product on pc.model = product.model
where pc.speed >= 750 and maker in (select  maker
                                    from laptop
                                        inner join product on laptop.model = product.model
where laptop.speed >= 750);

-- 24

select model
from (
         select price, model
         from pc
         union
         select price, model
         from laptop
         union
         select price, model
         from printer ) aa
where price = (
    select max(price)
    from (
             select price
             from pc
             union
             select price
             from laptop
             union
             select price
             from printer
         ) bb
);

-- 27
select maker,avg(hd)
from product
    inner join pc on product.model=pc.model
where maker in(select maker
from product
where type='printer')
group by maker;

-- 28
select count(*)
from ( select maker
from product
group by maker
having count(model) = 1 ) as Q;

-- 29
select a.point, a.date, inc, out
from income_o a left join outcome_o b on a.point = b.point
    and a.date = b.date
union
select b.point, b.date, inc, out
from income_o a right join outcome_o b on a.point = b.point
    and a.date = b.date;

-- 30
select point, date, sum(sum_out), sum(sum_inc)
from ( select point, date, sum(inc) as sum_inc, null as sum_out from income group by point, date
    union
    select point, date, null as sum_inc, sum(out) as sum_out from outcome group by point, date ) as t
group by point, date order by point;

-- 31
select class, country
from classes
where bore >= 16;

-- 33
select ship
from outcomes,battles
where result= 'sunk' and battle = 'North Atlantic'
group by ship;

-- 34
select name
from classes, ships
where launched >= 1922 and displacement > 35000 and type='bb' and ships.class = classes.class;

-- 38
select country
from classes
group by country
having count(distinct type) = 2;

-- 40
select maker, max(type)
from product
group by maker
having count(distinct type) = 1 and count(model) > 1;

-- 41
select ship, battle
from outcomes
where result = 'sunk';

-- 44
select name from ships
where name like 'R%'
union
select ship from outcomes
where ship like 'R%';

--50
select distinct battle from outcomes
where ship in (select name
               from ships
               where class = 'kongo');

--53
select round(avg(numGuns),2)
from classes
where type='bb';

--