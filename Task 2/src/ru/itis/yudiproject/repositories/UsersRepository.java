package ru.itis.yudiproject.repositories;

import ru.itis.yudiproject.models.User;

import java.util.UUID;

public interface UsersRepository extends CrudRepository<UUID, User> {
}
