package ru.itis.yudiproject.repositories;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

import ru.itis.yudiproject.dto.SignUpForm;
import ru.itis.yudiproject.models.User;

public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;
    private long counter;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
        this.counter = 0;
    }

    @Override
    public List<User> findAll() {
        String string = "";
        List<User> users = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            while ((string = bufferedReader.readLine()) != null) {
                String[] parameters = string.split("\\|");
                UUID id = UUID.fromString(parameters[0]);
                users.add(new User(id, parameters[1], parameters[2], parameters[3], parameters[4]));
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public void save(User entity) {
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        String userAsString = userToString.apply(entity);
        String string = "";
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            while ((string = bufferedReader.readLine()) != null) {
                if (!userAsString.equals(string)) {
                    stringBuilder.append(string).append("\n");
                }
            }

            string = stringBuilder.toString();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        char[] buffer = new char[string.length()];
        string.getChars(0, string.length(), buffer, 0);

        try (Writer writer = new FileWriter(fileName, false)) {
            User user = new User(UUID.randomUUID(), "Sofya", "Tyuleneva", "sofya.tyuleneva@gmail.com", "12345");
            writer.write(buffer);
            writer.write(userToString.apply(user));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(User entity) {
        String userAsString = userToString.apply(entity);
        String string = "";
        StringBuilder stringBuilder = new StringBuilder();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {

            while ((string = bufferedReader.readLine()) != null) {
                if (!userAsString.equals(string)) {
                    stringBuilder.append(string).append("\n");
                }
            }

            string = stringBuilder.toString();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        char[] buffer = new char[string.length()];
        string.getChars(0, string.length(), buffer, 0);

        try (Writer writer = new FileWriter(fileName, false)) {
            writer.write(buffer);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteById(UUID id) {
        String idAsString = id.toString();
        String string = "";
        StringBuilder stringBuffer = new StringBuilder();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {

            while ((string = bufferedReader.readLine()) != null) {
                String word = string.substring(0, string.indexOf("|"));
                if (!word.equals(idAsString)) {
                    stringBuffer.append(string).append("\n");
                }
            }

            string = stringBuffer.toString();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        char[] buffer = new char[string.length()];
        string.getChars(0, string.length(), buffer, 0);

        try (Writer writer = new FileWriter(fileName, false)) {
            writer.write(buffer);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User findById(UUID id) {
        String idAsString = id.toString();
        String string = "";
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            while ((string = bufferedReader.readLine()) != null) {
                String idUser = string.substring(0, string.indexOf("|"));
                if (idUser.equals(idAsString)) {
                    String[] items = string.split("\\|");
                    return new User(UUID.fromString(idUser), items[1], items[2], items[3], items[4]);
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }
}
