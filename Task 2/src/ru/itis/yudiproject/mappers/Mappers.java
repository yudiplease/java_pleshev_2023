package ru.itis.yudiproject.mappers;

import ru.itis.yudiproject.dto.SignUpForm;
import ru.itis.yudiproject.models.User;

public class Mappers {
    public static User fromSignUpForm(SignUpForm form) {
        return new User(form.getFirstName(), form.getLastName(), form.getEmail(), form.getPassword());
    }
}