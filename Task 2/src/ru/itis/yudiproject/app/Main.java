package ru.itis.yudiproject.app;


import ru.itis.yudiproject.dto.SignUpForm;
import ru.itis.yudiproject.mappers.Mappers;
import ru.itis.yudiproject.models.User;
import ru.itis.yudiproject.repositories.UsersRepository;
import ru.itis.yudiproject.repositories.UsersRepositoryFilesImpl;
import ru.itis.yudiproject.services.UsersService;
import ru.itis.yudiproject.services.UsersServiceImpl;

import java.util.List;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);
        SignUpForm signUpForm = new SignUpForm("Egor", "Usov", "egor.usov@gmail.com", "12345");
        SignUpForm signUpForm2 = new SignUpForm("Ilya123", "Pleshev", "dregoinger@gmail.com", "12345");
        SignUpForm signUpForm3 = new SignUpForm("Ilya456", "Pleshev", "dregoinger@gmail.com", "12345");
        usersService.signUp(signUpForm);
        usersService.signUp(signUpForm2);
        usersService.signUp(signUpForm3);
        User user = Mappers.fromSignUpForm(signUpForm);
        usersRepository.delete(user);
        UUID id = UUID.fromString("7e9a946f-1276-4021-88bb-4e0d1dad1174");
        User user2 = usersRepository.findById(id);
        System.out.println(user2.toString());
        List<User> list = usersRepository.findAll();
        System.out.println(list.toString());
        usersRepository.update(user2);
        UUID id2 = UUID.fromString("06ec0fd0-31de-4190-8ab3-e429a1d8ac2c");
        usersRepository.deleteById(id);
    }
}

