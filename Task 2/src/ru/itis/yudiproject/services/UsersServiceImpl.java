package ru.itis.yudiproject.services;

import java.util.function.Function;

import ru.itis.yudiproject.dto.SignUpForm;
import ru.itis.yudiproject.models.User;
import ru.itis.yudiproject.repositories.UsersRepository;

public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    private final Function<SignUpForm, User> toUserMapper;

    public UsersServiceImpl(UsersRepository usersRepository, Function<SignUpForm, User> toUserMapper) {
        this.usersRepository = usersRepository;
        this.toUserMapper = toUserMapper;
    }

    @Override
    public void signUp(SignUpForm signUpForm) {
        User user = toUserMapper.apply(signUpForm);

        // TODO: сделать проверку, чтобы пользователя с таким Email-не было

        usersRepository.save(user);
    }
}
