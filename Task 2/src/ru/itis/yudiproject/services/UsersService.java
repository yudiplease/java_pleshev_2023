package ru.itis.yudiproject.services;

import ru.itis.yudiproject.dto.SignUpForm;

public interface UsersService {
    void signUp(SignUpForm signUpForm);
}
