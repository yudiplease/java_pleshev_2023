drop table if exists client;
drop table if exists driver;
drop table if exists car;
drop table if exists taxi_order;

create table client (
                        id bigserial primary key,
                        first_name char(20),
                        last_name char(20),
                        age integer,
                        balance integer
);

create table driver (
                        id bigserial primary key,
                        first_name char(20),
                        last_name char(20),
                        age integer check ( age > 18 and age < 60 ),
                        rating integer,
                        count_trips integer
);

create table car (
                     id bigserial primary key,
                     car_brand char(20),
                     vehicle_numbers char(20),
                     child_safety_seat boolean,
                     color char(20)
);

create table taxi_order (
                            id bigserial primary key,
                            boarding_point char(100),
                            drop_off_point char(100),
                            price integer,
                            distance_km integer,
                            rate char(20)
);

alter table client
    add rating integer not null default 0;

alter table driver
    add issue_driving_license date,
    add end_driving_license date;

alter table client
    add taxi_order_id bigint;

alter table client
    add foreign key (taxi_order_id) references taxi_order(id);
