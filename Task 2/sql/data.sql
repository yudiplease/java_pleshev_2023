insert into client(first_name, last_name, age, balance)
values ('Илья', 'Плешев', 18, 350),
       ('Руслан', 'Виолов', 20, 500),
       ('Софья', 'Тюленева', 16, 1000),
       ('Анастасия', 'Шаврова', 16, 1000);

insert into driver(first_name, last_name, age, rating, count_trips, issue_driving_license, end_driving_license)
values ('Марк', 'Твен', 25, 5, 115, '2015-02-02', '2025-02-02'),
       ('Адель', 'Апчихдуллин', 40, 3, 465, '2020-02-02', '2030-02-02'),
       ('Лев', 'Толстой', 55, 4, 1150, '2014-02-02', '2024-02-02'),
       ('Александр', 'Пушкин', 30, 5, 279, '2015-02-02', '2025-02-02');

insert into car(car_brand, color)
values ('Toyota', 'Белый'),
       ('BMW', 'Чёрный'),
       ('Mercedes', 'Серый'),
       ('Mitsubishi', 'Красный');

insert into taxi_order(price, distance_km)
values (115, 3),
       (249, 5),
       (109, 2),
       (300, 6);

update car
set vehicle_numbers   = 'О777ОО',
    child_safety_seat = true
where id = 1;

update car
set vehicle_numbers   = 'Е666ЕЕ',
    child_safety_seat = false
where id = 2;

