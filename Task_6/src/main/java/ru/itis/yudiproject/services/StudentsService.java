package ru.itis.yudiproject.services;

import ru.itis.yudiproject.dto.StudentSignUp;

public interface StudentsService {
    void signUp(StudentSignUp form);
}


