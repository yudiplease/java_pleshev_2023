package ru.itis.yudiproject;

import ru.itis.yudiproject.jdbc.SimpleDataSource;
import ru.itis.yudiproject.models.Student;
import ru.itis.yudiproject.repositories.StudentsRepository;
import ru.itis.yudiproject.repositories.StudentsRepositoryJdbcTemplateImpl;
import ru.itis.yudiproject.services.StudentsService;
import ru.itis.yudiproject.services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcTemplateImpl(dataSource);

        Student student = Student.builder()
                .firstName("Hi!")
                .lastName("Bye")
                .email("email")
                .password("qwerty0")
                .build();

        System.out.println(student);
        studentsRepository.save(student);
        List<Student> students = studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(23);
        System.out.println(students.toString());
        studentsRepository.delete(1L);
        studentsRepository.update(student);
        System.out.println(student);
    }
}